/**
 * Copyright © Swiss Clinic, Inc. All rights reserved.
 */
define([
    "jquery",
    "matchMedia",
    'underscore',
    "jquery/ui",
    "jquery/jquery.mobile.custom",
    "mage/translate"
], function ($, mediaCheck, _) {
    "use strict";

    var mouseHandled = false;

    $.widget("swissclinic.header", {
        delay: 500,
        options: {
            classes: {},
            disabled: false,
            delay: 500,
            responsive: true
        },
        timeout: null,

        _create: function () {
        },

        _init: function () {


            this.delay = this.options.delay;


            this._assignControls();

            mediaCheck({
                media: "(max-width: 767px)",
                entry: $.proxy(function () {
                    this._toggleMobileMode();
                }, this),
                exit: $.proxy(function () {
                    this._toggleDesktopMode();
                }, this)
            });
        },

        _assignControls: function () {
            this.controls = {
                tabButtons: $("#main-menu [data-target]"),
                tabButtonsContainer: $("#main-menu .navigation--main-menu"),
                subMenuContainer: $("#main-menu .modal-menu__col--sub-menu"),
                tabs: $("#main-menu [data-content]")
            };

            return this;
        },

        _hideTabs: function () {
            var self = this;
            if (self.controls.tabs.length > 1) {
                self.controls.tabs.each(function (index, el) {
                    $(el).removeClass('active');
                    $(el).css("display", "none");
                });
            }
            return self;
        },

        _setPaddingTabContents: function() {
            var self = this;

            self.controls.tabs.each(function (index, el) {
                var target = $(this).data('content'),
                    tabElement = $('[data-target="' + target + '"]')[0];
                $(el).css({"display": null, 'left': tabElement.getBoundingClientRect().left + 'px'});
            });
        },

        _clearTabsButtons: function () {
            var self = this;
            if (self.controls.tabButtons.length > 1) {
                self.controls.tabButtons.each(function (index, el) {
                    $(el).removeClass("active");
                });
            }
            return self;
        },

        toggleSearch: function () {
        },

        _openSubMenu: function () {
            var self = this;
            var height = 0;
            self.controls.tabs.each(function () {
                var tabHeight = this.getBoundingClientRect().height;
                if (tabHeight > height) {
                    height = this.getBoundingClientRect().height;
                }
            });

            self.controls.subMenuContainer.css("height", height + 48 + "px");
        },

        _closeSubMenu: function () {
            var self = this;
            self.controls.subMenuContainer.css("height", "0");
            self._hideTabs();
            self.timeout = null;
        },

        _toggleMobileMode: function () {
            var self = this;

            this.controls.tabButtons.off("mouseleave");
            this.controls.tabButtons.off("mouseenter");
            this.controls.subMenuContainer.off("mouseenter");
            this.controls.subMenuContainer.off("mouseleave");
            $(window).off("resize");


            this.controls.tabs.each(function (index, el) {
                $(el).removeAttr('style');
                if (index > 0) {
                    $(el).css("display", "none");
                } else {
                    $(el).css("display", "block");
                }
            });

            $(this.controls.tabButtons[0]).addClass("active");

            this.controls.tabButtons.on("click", function (e) {
                e.preventDefault();
                var target = $(this).data("target");
                self._hideTabs();
                self._clearTabsButtons();
                $(this).addClass("active");
                $('[data-content="' + target + '"]').css("display", "block");
            });
        },

        _setTimeout: function () {
            var self = this;

            if (!self.timeout) {
                self.timeout = setTimeout(function () {
                    self._closeSubMenu()
                }, self.delay);
            }
        },

        _toggleDesktopMode: function () {
            var self = this;

            jQuery("#main-menu").removeAttr("style");

            self._setPaddingTabContents();

            $(window).resize( _.debounce( self._setPaddingTabContents.bind(self), 500 ));

            self.controls.tabButtons.off("click");

            self.controls.tabButtons.on("mouseenter", function(e) {
                var target = $(this).data('target');

                self._hideTabs();

                $('[data-content="' + target + '"').css('display', 'block');

                if (self.timeout) {
                    clearTimeout(self.timeout);
                    self.timeout = null;
                }

                self._openSubMenu();
            });

            self.controls.subMenuContainer.on("mouseenter", function () {
                if (self.timeout) {
                    clearTimeout(self.timeout);
                    self.timeout = null;
                    return;
                }
            });

            self.controls.subMenuContainer.on("mouseleave", function () {
                self._setTimeout();
            });

            self.controls.tabButtons.on("mouseleave", function () {
                self._setTimeout();
            });
        }
    });

    return $.swissclinic.header;
});
