define([
    'jquery',
    'underscore',
    'Magento_Ui/js/modal/alert',
    'Magento_Customer/js/customer-data',
    'jquery-ui-modules/widget',
    'mage/cookies',
], function ($, _, alert, customerData) {
    'use strict';

    $.widget('sc.miniCart', {
        options: {
            isRecursive: true,
            minicart: {
                maxItemsVisible: 3
            }
        },
        options: {
            selectors: {
                remove: '.product__remove a.action.delete',
                product: '.product',
                toCheckout: '.mini-cart__actions .button.button--primary',
                continueShopping: '.mini-cart__actions .button.button--secondary',
            },
            url: {
                checkout: window.checkout.checkoutUrl,
                shoppingCart: window.checkout.shoppingCartUrl,
                update: window.checkout.updateItemQtyUrl,
                remove: window.checkout.removeItemUrl,
                loginUrl: window.checkout.customerLoginUrl,
                isRedirectRequired: window.checkout.isRedirectRequired
            },
        },

        _create: function () {
            this._bind()
        },

        _bind: function () {
            var events = {};

            events['click ' + this.options.selectors.remove] =  function (event) {
                var self = this;
                event.stopPropagation();
                self._removeItem($(event.currentTarget));
            };

            events['click ' + this.options.selectors.toCheckout] =  function (event) {
                var cart = customerData.get('cart'),
                    customer = customerData.get('customer');

                if (!customer().firstname && cart().isGuestCheckoutAllowed === false) {
                    // set URL for redirect on successful login/registration. It's postprocessed on backend.
                    $.cookie('login_redirect', this.options.url.shoppingCart);

                    if (this.options.url.isRedirectRequired) {
                        location.href = this.options.url.loginUrl;
                    }

                    return false;
                }
                location.href = this.options.url.shoppingCart;
            };

            events['click ' + this.options.selectors.continueShopping] =  function (event) {
                $('#mini-cart .modal-menu').modalMenu('close')
            };

            this._on(this.element, events)
        },

        _getProductById: function (productId) {
            return _.find(customerData.get('cart')().items, function (item) {
                return productId === Number(item['item_id']);
            });
        },

        _removeItem: function (elem) {
            var itemId = elem.data('cart-item');

            this._ajax(this.options.url.remove, {
                'item_id': itemId
            }, elem, this._removeItemAfter);
        },

        _removeItemAfter: function (elem) {
            var productData = this._getProductById(Number(elem.data('cart-item')));

            if (!_.isUndefined(productData)) {
                $(document).trigger('ajax:removeFromCart', {
                    productIds: [productData['product_id']]
                });

                if (window.location.href.indexOf(this.shoppingCartUrl) === 0) {
                    window.location.reload();
                }
            }
        },

        _ajax: function (url, data, elem, callback) {
            $.extend(data, {
                'form_key': $.mage.cookies.get('form_key')
            });

            $.ajax({
                url: url,
                data: data,
                type: 'post',
                dataType: 'json',
                context: this,

                /** @inheritdoc */
                beforeSend: function () {
                    elem.attr('disabled', 'disabled');
                },

                /** @inheritdoc */
                complete: function () {
                    elem.attr('disabled', null);
                }
            })
                .done(function (response) {
                    var msg;

                    if (response.success) {
                        callback.call(this, elem, response);
                    } else {
                        msg = response['error_message'];

                        if (msg) {
                            alert({
                                content: msg
                            });
                        }
                    }
                })
                .fail(function (error) {
                    console.log(JSON.stringify(error));
                });
        },
    });

    return $.sc.miniCart;
});
