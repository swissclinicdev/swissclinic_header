define([
    'jquery',
    "matchMedia",
    'jquery-ui-modules/widget'
], function ($, mediaCheck) {
    'use strict';

    $.widget('sc.modalMenu', {
        options: {
            selectors: {
                close: '.modal-menu__close'
            }
        },

        _init: function () {
            this._bind()
        },

        _bind: function () {
            var self = this;

            $(self.options.selectors.close).on('click', function () {
                self._close()
            });

            $(self.options.toggle).on('click', function () {
                self._toggle()
            });

            mediaCheck({
                media: "(max-width: 767px)",
                entry: $.proxy(function () {
                    $(self.element).on('transitionend', function () {
                        if (!self.element.hasClass('open')) {
                            self.element.css('display', 'none');
                        }
                    });
                }, this),
                exit: $.proxy(function () {
                    $(self.element).off('transitionend');
                }, this)
            });
        },

        _close: function () {
            var self = this;
            self.element.removeClass('open')
        },

        _toggle: function () {
            var self = this;

            if (!self.element.hasClass('open')) {
                self.element.css('display', 'flex')
            }

            setTimeout(function () {
                self.element.toggleClass('open')
            }, 20)
        },

        open: function () {
            var self = this;

            if (!self.element.hasClass('open')) {
                self.element.css('display', 'flex');
                setTimeout(function () {
                    self.element.toggleClass('open')
                }, 20)
            }
        },

        close: function () {
            var self = this;

            if (self.element.hasClass('open')) {
                self.element.removeClass('open')
            }
        }

    });

    return $.sc.sidebar;
});
