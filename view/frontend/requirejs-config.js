var config = {
    map: {
        "*": {
            "swissclinic/modal_menu": "Swissclinic_Header/js/modal-menu",
            "swissclinic/header": "Swissclinic_Header/js/header",
            "swissclinic/minicart": "Swissclinic_Header/js/mini-cart",
            "catalogAddToCart": "Swissclinic_Header/js/catalog-add-to-cart",
            "Magento_Catalog/js/catalog-add-to-cart": "Swissclinic_Header/js/catalog-add-to-cart",
        }
    }
};
