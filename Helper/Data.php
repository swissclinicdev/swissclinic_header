<?php

namespace Swissclinic\Header\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected const XML_PATH_SWISSCLINIC_HEADER_TABS_BLOCK= 'swissclinic_header/header_blocks/tabs_block';
    protected const XML_PATH_SWISSCLINIC_HEADER_TABS_CONTENT_BLOCK= 'swissclinic_header/header_blocks/tabs_content_block';

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * @param null $storeId
     * @return mixed
     */
    public function getTabsBlockId($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_SWISSCLINIC_HEADER_TABS_BLOCK,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @param null $storeId
     * @return mixed
     */
    public function getTabsContentBlockId($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_SWISSCLINIC_HEADER_TABS_CONTENT_BLOCK,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

}