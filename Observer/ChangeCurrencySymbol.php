<?php
/**
 * Copyright 2019 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Swissclinic\Header\Observer;

use Magento\Framework\Event\ObserverInterface;  

class ChangeCurrencySymbol implements ObserverInterface
{

	protected $_storeManager;    
    
    public function __construct(     
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {        
        $this->_storeManager = $storeManager;
    }

    public function getStoreCode()
    {
        return $this->_storeManager->getStore()->getCode();
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {    
        $currencyOptions = $observer->getEvent()->getCurrencyOptions();
        //chnage currency symbol for the Swiden store
        if ($this->getStoreCode() == 'se') {
        	# code...
        	$currencyOptions->setData('symbol', 'SEK');  
        }
        return $this;
    }    
}