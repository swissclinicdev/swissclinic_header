<?php
/**
 * Copyright 2019 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Swissclinic\Header\Observer;

use Swissclinic\Header\Helper\Data as Helper;
use Magento\Framework\Escaper;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\StoreManager;

/**
 * Class AddLayoutUpdate
 * @package Aheadworks\Sbp\Observer
 */
class MenuObserver implements ObserverInterface
{

    protected $_escaper;

    protected $_layout;

    protected $_storeManager;

    protected $_helper;

    protected $_blockFactory;

    public function __construct(
        Escaper $escaper,
        StoreManager $storeManager,
        Helper $helper,
        \Magento\Framework\View\LayoutInterface $layout,
        \Magento\Cms\Model\BlockFactory $blockFactory
    )
    {
        $this->_escaper = $escaper;
        $this->_layout = $layout;
        $this->_storeManager = $storeManager;
        $this->_helper = $helper;
        $this->_blockFactory = $blockFactory;
    }

    protected function _getStaticContent($store_id = null, $type = null) {

        if (!$type) {
            return '';
        }

        if ($store_id == null) {
            $store_id = $this->_storeManager->getStore()->getId();
        }
        $block_id = '';

        if ($type === 'tabs') {
            $block_id = $this->_helper->getTabsBlockId($store_id);
        }

        if ($type === 'content') {
            $block_id = $this->_helper->getTabsContentBlockId($store_id);
        }

        $block = $this->_layout->createBlock('Magento\Cms\Block\Block')->setStoreId($store_id)->setBlockId($block_id);

        return $block->toHtml();
    }


    protected function _addSubMenu($child, $store_id = null)
    {

        $html = '';
        $children = $child->getChildren();

        if ($children->count() === 0) {
            return '';
        }

        $parentName = $this->_escaper->escapeHtml($child->getName());
        $html .= '<div class="navigation-tab" data-content="' . strtolower(str_replace(" ", "-", $parentName)) . '">';
        $html .= '<ul class="navigation navigation--submenu">';
        foreach ($children as $child) {
            $html .= '<li class="navigation-item" title="' . $this->_escaper->escapeHtml($child->getName()) .'" itemscope itemtype="https://www.schema.org/SiteNavigationElement">';
            $html .= '<a href="' . $child->getUrl() . '" itemprop="name" class="navigation-link">' . $this->_escaper->escapeHtml( $child->getName() ) . '</a></li>';

        }
        $html .= '</ul></div>';

        return $html;
    }

    protected function _renderDataTarget($child)
    {
        $dataTargetAttribute = '';

        if ($child->getChildren()->count() === 0) {
            return $dataTargetAttribute;
        }

        $dataTargetAttribute .= 'data-target="' . strtolower(str_replace(" ", "-", $this->_escaper->escapeHtml($child->getName()))) . '"';

        return $dataTargetAttribute;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $store_id = $this->_storeManager->getStore()->getId();
        $tabsHtml = $this->_getStaticContent($store_id, 'tabs');
        $tabsContentHtml = $this->_getStaticContent($store_id, 'content');
        $menu = $observer->getData('menu');
        $transportObject = $observer->getData('transportObject');
        $topLinksBlock = $this->_layout->getBlock('top.links');
        $html = '';
        $firstSectionHtml = '
        <div class="modal-menu__inner">
            <div class="modal-menu__col modal-menu__col--navigation-bar">
                <div class="modal-menu__section modal-menu__section--main-menu">
                    <ul class="navigation navigation--main-menu">
        ';

        $secondSectionHtml = '<div class="modal-menu__col modal-menu__col--sub-menu"><div class="modal-menu__section">';

        $children = $menu->getChildren();

        foreach ($children as $child) {
            $firstSectionHtml .= '<li class="navigation-item" title="' . $this->_escaper->escapeHtml($child->getName()) .'" itemscope itemtype="https://www.schema.org/SiteNavigationElement">';
            $firstSectionHtml .= '<a href="' . $child->getUrl() . '" itemprop="name" ' . $this->_renderDataTarget($child) . ' >' . $this->_escaper->escapeHtml( $child->getName() ) . '</a></li>';
            $secondSectionHtml .= $this->_addSubMenu($child, $store_id);
        }

        $html .= $firstSectionHtml . $tabsHtml . '</ul></div><div class="modal-menu__section modal-menu__section--links">';

        if ($topLinksBlock) {
            $html .= $topLinksBlock->toHtml();
        }

        $html .= '</div></div>';

        $html .= $secondSectionHtml;

        if ($tabsContentHtml) {
            $html .= $tabsContentHtml;
        }
        $html .= '</div></div></div>';

        $transportObject->setHtml($html);
    }
}
