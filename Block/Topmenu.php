<?php

namespace Swissclinic\Header\Block;

use Magento\Store\Model\ScopeInterface;

class Topmenu extends \Magento\Framework\View\Element\Template
{
    protected $_store;
    protected $_storeManager;
    protected $_categoryHelper;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Helper\Category $categoryHelper,   
        array $data = []
    ) {
        $this->_storeManager = $storeManager;
        $this->_store = $storeManager->getStore();
        $this->_categoryHelper = $categoryHelper;
        parent::__construct($context, $data);
    }

    public function getStoreCategories($sorted = false, $asCollection = false, $toLoad = true)
    {
        $categories = array();

        $categoryCollection = $this->_categoryHelper->getStoreCategories($sorted , $asCollection, $toLoad);

        foreach ($categoryCollection as $category) {

            $currentCategory = array("name"=>$category->getName(), "url"=>$this->_categoryHelper->getCategoryUrl($category));

            array_push($categories, $currentCategory);
        }

        return $categories;
    }

}

?>